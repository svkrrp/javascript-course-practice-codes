const task3Element = document.getElementById('task-3');

function greetFunction() {
    alert('hello there!');
}
function greetName(name) {
    alert(name);
}

greetFunction();
greetName('shivam');

task3Element.addEventListener('click', greetFunction);

function concatenate(str1, str2, str3) {
    const result = str1 + ' ' + str2 + ' ' + str3;
    return result;
}
alert(concatenate('hey', 'what', 'is'));