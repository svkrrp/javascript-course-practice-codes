const sayHello = (name) => console.log('Hi ' + name);
sayHello('Shivam');

// task-2
const sayHelloWith2Param = (greet, name) => {
  console.log(`${greet} ${name}`);
}

const sayHelloWihNoParam = () => 'hi shivam';

sayHelloWith2Param('hi', 'shivam');
console.log(sayHelloWihNoParam());

// Task 3
const sayHello2 = (name, greet = 'Hi') => {
  console.log(greet + ' ' + name);
}
sayHello2('shivam');

// Task 4
const checkInput = (stringHandler, ...strings) => {
  if(strings.length == 0) {
    stringHandler('empty string');
  }else {
    for(const string of strings) {
      console.log(string);
    }
  }
}

const emptyString = (message) => {
  console.log(message);
}

checkInput(emptyString);