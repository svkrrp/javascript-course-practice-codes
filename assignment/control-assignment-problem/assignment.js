const randomNumber = Math.random(); // produces random number between 0 (including) and 1 (excluding)

// task 1
if (randomNumber > 0.7) {
    alert('hey number is greater then 0.7');
}

// task 2
let arrayOfNumbers = [2, 4, 3, 8];

for(let i=0; i<arrayOfNumbers.length; i++) {
    console.log(arrayOfNumbers[i]);
}

let i=0;
while(i < arrayOfNumbers.length) {
    console.log(arrayOfNumbers[i]);
    i++;
}

for(const num of arrayOfNumbers) {
    console.log(num);
}

// task 3
for(let i=arrayOfNumbers.length-1; i>=0; i--) {
    console.log(arrayOfNumbers[i]);
}

// task 3
const anotherRandomNumber = Math.random();
console.log(anotherRandomNumber + " " + randomNumber);
if(anotherRandomNumber > 0.7 && randomNumber > 0.7) {
    alert('both');
} else if (anotherRandomNumber < 0.2 || randomNumber < 0.2) {
    alert('not greater than 0.2');
}